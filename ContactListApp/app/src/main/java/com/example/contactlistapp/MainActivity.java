package com.example.contactlistapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int CODE_RES = 1;                              //Request code for Activity results
    private ArrayList<Contact> contactList =  new ArrayList<Contact>(); //To keep our contacts


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button)findViewById(R.id.addContact);          //Launches our second activity to get the contact informations
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactAdd.class);

                startActivityForResult(intent,CODE_RES);

            }
        });

        ArrayAdapter<Contact> adapter = new ArrayAdapter<Contact>(      //Sets our listview to display our contacts from the array
                this,
                android.R.layout.simple_list_item_1,
                contactList
        );

        ListView listView = (ListView)findViewById(R.id.listContact);
        listView.setAdapter(adapter);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode){
            case CODE_RES:
                switch (resultCode){
                    case RESULT_OK:

                        //If the user clicked add
                        if(data != null){
                            String nom = data.getStringExtra("contact_nom"),
                                    prenom = data.getStringExtra("contact_prenom"),
                                    numTel = data.getStringExtra("contact_numTel");

                            //Creates a new Contact with our provided data
                            Contact contact = new Contact(
                                    nom,
                                    prenom,
                                    numTel
                            );

                            //Adds the new Contact to the array
                            contactList.add(contact);

                            //A confirmation message to show that the Contact as been added
                            Toast.makeText(this,"Contact ajouté !", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case RESULT_CANCELED:
                        //User canceled his action / Activity crashed
                        Toast.makeText(this, "Action annulée", Toast.LENGTH_LONG).show();
                        break;
                }
                break;
            default:
                break;
        }
    }
}
