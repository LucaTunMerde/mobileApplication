package com.example.contactlistapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ContactAdd extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_add);
    }

    @Override
    public void onClick(View v){
        //Creates variables with our user input data
        EditText editText = (EditText)findViewById(R.id.nom);
        String nom = editText.getText().toString();

        editText = (EditText)findViewById(R.id.prenom);
        String prenom = editText.getText().toString();

        editText = (EditText)findViewById(R.id.numTel);
        String numTel = editText.getText().toString();

        //Put our contact data in an response intent
        Intent intent = new Intent();
        intent.putExtra("contact_nom",nom);
        intent.putExtra("contact_prenom",prenom);
        intent.putExtra("contact_numTel",numTel);


        switch (v.getId()){
            case R.id.addButton:

                //If the user clicked "add" sends the data to the main
                setResult(RESULT_OK,intent);
                finish();
                break;
            case R.id.cancelButton:

                //User aborted the action
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }
}
