package com.example.contactlistapp_withDb;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int CODE_RES = 1;                              //Request code for Activity results
    private ArrayList<Contact> contactList = new ArrayList<Contact>(); //To keep our contacts

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Creates a new database linker
        Database dbHelper = new Database(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        //An array of the columns we want
        String[] projection = {"id", "nom", "prenom", "numTel"};

        //Gets all our contacts
        Cursor cursor = db.query(
                "contacts",   // The table to query
                projection,             // The array of columns to return
                "",              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                " nom, prenom "               // The sort order
        );

        //Adds every contact from the database to our contact list
        while (cursor.moveToNext()) {
            String nom = cursor.getString(cursor.getColumnIndexOrThrow("nom"));
            String prenom = cursor.getString(cursor.getColumnIndexOrThrow("prenom"));
            String numTel = cursor.getString(cursor.getColumnIndexOrThrow("numTel"));
            Contact contact = new Contact(nom, prenom, numTel);
            contactList.add(contact);
        }
        cursor.close();

        Button button = (Button) findViewById(R.id.addContact);          //Launches our second activity to get the contact informations
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactAdd.class);

                startActivityForResult(intent, CODE_RES);

            }
        });

        ArrayAdapter<Contact> adapter = new ArrayAdapter<Contact>(      //Sets our listview to display our contacts from the array
                this,
                android.R.layout.simple_list_item_1,
                contactList
        );

        ListView listView = (ListView) findViewById(R.id.listContact);
        listView.setAdapter(adapter);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CODE_RES:
                switch (resultCode) {
                    case RESULT_OK:

                        //If the user clicked add
                        if (data != null) {
                            String nom = data.getStringExtra("contact_nom"),
                                    prenom = data.getStringExtra("contact_prenom"),
                                    numTel = data.getStringExtra("contact_numTel");

                            //Creates a new Contact with our provided data
                            Contact contact = new Contact(
                                    nom,
                                    prenom,
                                    numTel
                            );

                            //Adds the new Contact to the array
                            contactList.add(contact);

                            Database dbHelper = new Database(this);
                            SQLiteDatabase db = dbHelper.getWritableDatabase();

                            // Create a new map of values, where column names are the keys
                            ContentValues values = new ContentValues();
                            values.put("nom", nom);
                            values.put("prenom", prenom);
                            values.put("numTel", numTel);

                            // Insert the new row
                            db.insert("contacts", null, values);

                            //A confirmation message to show that the Contact as been added
                            Toast.makeText(this, "Contact ajouté !", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case RESULT_CANCELED:
                        //User canceled his action / Activity crashed
                        Toast.makeText(this, "Action annulée", Toast.LENGTH_LONG).show();
                        break;
                }
                break;
            default:
                break;
        }
    }
}