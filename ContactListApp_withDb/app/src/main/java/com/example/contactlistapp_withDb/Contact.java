package com.example.contactlistapp_withDb;

import androidx.annotation.NonNull;

public class Contact {
    private String nom,prenom,numeroTel;

    Contact(String nom, String prenom, String numeroTel) {
        this.nom = nom;
        this.prenom = prenom;
        this.numeroTel = numeroTel;
    }


    //Re-writing the toString function to display all values in the listView (and not the pointer)
    @NonNull
    @Override
    public String toString() {
        return nom + " " + prenom + " " + numeroTel;
    }
}
