package com.example.contactlistapp_withFile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int CODE_RES = 1;                              //Request code for Activity results
    private ArrayList<Contact> contactList =  new ArrayList<Contact>(); //To keep our contacts

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Reads our contact file
        String file = readFromFile(this, "contacts.txt");

        //Puts in an array every contact listed in the contact file
        String[] contacts = file.split(" && ");

        //For every contact
        for (String contact: contacts) {
            //Separates the informations from our contact
            String[] infosContact = contact.split(" ");

            //Creates a new contact with our informations
            Contact contact1 = new Contact(infosContact[0],infosContact[1],infosContact[2]);
            //Adds the contact to our list
            contactList.add(contact1);
        }

        Button button = (Button)findViewById(R.id.addContact);          //Launches our second activity to get the contact informations
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactAdd.class);

                startActivityForResult(intent,CODE_RES);

            }
        });

        ArrayAdapter<Contact> adapter = new ArrayAdapter<Contact>(      //Sets our listview to display our contacts from the array
                this,
                android.R.layout.simple_list_item_1,
                contactList
        );

        ListView listView = (ListView)findViewById(R.id.listContact);
        listView.setAdapter(adapter);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode){
            case CODE_RES:
                switch (resultCode){
                    case RESULT_OK:

                        //If the user clicked add
                        if(data != null){
                            String nom = data.getStringExtra("contact_nom"),
                                    prenom = data.getStringExtra("contact_prenom"),
                                    numTel = data.getStringExtra("contact_numTel");

                            //Creates a new Contact with our provided data
                            Contact contact = new Contact(
                                    data.getStringExtra("contact_nom"),
                                    data.getStringExtra("contact_prenom"),
                                    data.getStringExtra("contact_numTel")
                            );

                            //Adds the new Contact to the array
                            contactList.add(contact);
                            writeToFile(contact.toString(),this);

                            //A confirmation message to show that the Contact as been added
                            Toast.makeText(this,"Contact ajouté !", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case RESULT_CANCELED:
                        //User canceled his action / Activity crashed
                        Toast.makeText(this, "Action annulée", Toast.LENGTH_LONG).show();
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void writeToFile(String data, Context context) {
        try {
            //Writes in contact.txt the data
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("contacts.txt", Context.MODE_APPEND));
            outputStreamWriter.write(data);

            //Adds a separator the distinguish our differents datas
            outputStreamWriter.write(" && ");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Toast.makeText(this, "File write failed"+ e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    private String readFromFile(Context context, String name) {

        String ret = "";

        try {

            //Opens the specified file
            InputStream inputStream = context.openFileInput(name);

            if ( inputStream != null ) {
                //Reads the file
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                //Writes in a response string the file's data
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Toast.makeText(this, "File not found "+ e.toString(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(this, "Can not read file "+ e.toString(), Toast.LENGTH_LONG).show();
        }

        return ret;
    }
}
